# atlasboard-bcel-package

# Dev

##Pour créer un container local de développement : 

export BOARD_HOME=<project_home>

sudo docker run --rm \
                -ti --hostname atlasboard --name atlasboard \
                -p 3000:3000 \
                --env NEXUS_NPM_TOKEN=${NEXUS_NPM_TOKEN}= \
                --env AWS_HOST=${AWS_HOST} \
                --env NEXUS_URL_NPM_REPO=${NEXUS_URL_NPM_REPO} \
                -v $BOARD_HOME/node_modules:/home/node/teamboard/node_modules/:rw \
                -v $BOARD_HOME/node_modules:/home/node/teamboard/packages/atlassian/node_modules/:rw \
                -v $BOARD_HOME/node_modules:/home/node/teamboard/packages/red6/node_modules/:rw \
                -v $BOARD_HOME/node_modules:/home/node/teamboard/packages/bcel/node_modules/:rw \
                -v $BOARD_HOME/:/home/node/teamboard/packages/bcel/:rw \
                -v $BOARD_HOME/dashboard:/home/node/teamboard/packages/default/dashboards/:rw \
                bcelouest/docker-atlasboard bash

## Dans le container démarré, il faut lancer la commande

$> atlasboard start

## Ouvrir un navigateur

Adresse : http://localhost:3000
