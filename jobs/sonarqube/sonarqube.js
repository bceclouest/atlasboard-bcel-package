const util = require('util');

/**
 * Job: dory-filter
 *
 * Expected configuration:
 *
 * {
 *     "interval": 1800000,
 *     "supergreen" : "images/super_green.jpg",
 *     "team_manual_measure": 1,
 *     "days_since_last_commitMax": 30,
 *     "credentials": "dory",
 *     "widgetTitle": "Quality Gate Failures",
 *     "filter": "only-errors" // or "passed",
 *     "externalURL": "" // url to used for links
 *     "showMajorViolations": "false",
 *     "onlyQualityGate": "true"
 * }
 */
module.exports = {

    onRun: function (config, dependencies, job_callback) {
        //config.debug = true;

        fetchListOfProjects(config, dependencies)
            .then(function (projects) {
                if (config.debug) {
                    util.log(JSON.stringify(projects, null, 2));
                }
                return fetchProjectsMeasures(config, dependencies, projects);
            })
            .then(decodeProjectsMeasures)
            .then(function (projects) {
                if (config.debug) {
                    util.log(JSON.stringify(projects, null, 2));
                }
                const millisecondsPerDay = 24 * 60 * 60 * 1000;
                const timeHorizon = Date.now() - config.days_since_last_commitMax * millisecondsPerDay;
                if (config.filter === "passed") {
                    return filterProjectsWithStatus(config, projects, timeHorizon, 
                        project => {
                            if (project.alert_status === 'ERROR') {
                                return false;
                            }
                            if (config.onlyQualityGate) {
                                return true;
                            }
                            if (project.critical_violations === 0
                                && project.blocker_violations === 0) {
                                return true;
                            }
                            return false;
                        },
                        level =>
                            level !== 'ERROR'
                    );
                } else if (config.filter === "only-errors") {
                    // return filterProjectsInError(projects, timeHorizon);
                    return filterProjectsWithStatus(config, projects, timeHorizon,
                        project => {
                            if (project.alert_status === 'ERROR') {
                                return true;
                            }
                            if (config.onlyQualityGate) {
                                return false;
                            }
                            if (project.critical_violations > 0
                                && project.blocker_violations > 0) {
                                return true;
                            }
                            return false;
                        },
                            // // only projet in ERROR
                            // project.alert_status === 'ERROR'
                            // && (!config.onlyQualityGate || (
                            //     // or with critical or blocker violations
                            //     project.critical_violations > 0
                            //     || project.blocker_violations > 0),
                        level =>
                            level === 'ERROR'
                    );
                } else {
                    return new Promise(function (resolve, reject) {
                        resolve(projects);
                    });
                }
            })
            .then(function (filteredProjects) {
                if (config.debug) {
                    util.log("filteredProjects: " + JSON.stringify(filteredProjects, null, 2));
                }
                var statusOk = true;
                if (config.filter === "only-errors") {
                    statusOk = filteredProjects.length === 0 ? true : false;
                } else if (config.filter === "passed") {
                    statusOk = filteredProjects.length === 0 ? false : true;
                }
                if (config.debug) {
                    util.log("statusOk: " + statusOk);
                }
                // prepare an array, by project
                const projectsQA = Object.keys(filteredProjects)
                    .sort()
                    // .sort((n1, n2) => 
                    //     filteredProjects[n1].last_commit_date > filteredProjects[n2].last_commit_date ? -1 : 1)
                    .map(function (projectName) {
                        return {
                            name: projectName,
                            url: filteredProjects[projectName].url,
                            metrics: filteredProjects[projectName].metrics
                        };
                    });

                job_callback(null, {
                    title: config.widgetTitle,
                    statusOk: statusOk,
                    projects: projectsQA
                });


            })
            .catch(function (err) {
                job_callback(err.message);
            });
    }
};

function computeRequestOptionsForSQ(config, path) {
    var headers = {};
    if (config.globalAuth[config.credentials].username) {
        headers.authorization = 'Basic ' + new Buffer(
                config.globalAuth[config.credentials].username
                + ':'
                + config.globalAuth[config.credentials].password
            ).toString('base64');
    }
    return {
        url: config.globalAuth[config.credentials].rootURL + path,
        headers: headers
        , timeout: config.interval * 3
        , rejectUnauthorized: false
    };
}

module.exports.fetchListOfProjects = fetchListOfProjects;
function fetchListOfProjects(config, dependencies) {
    return new Promise(
        function (resolve, reject) {
        	const filter = config.filterProjects ? util.format("&%s", config.filterProjects) : '';
        	const path = '/api/projects/index?format=json' + filter;
            const options = computeRequestOptionsForSQ(config, path);
            if (config.debug) {
                console.log(JSON.stringify(options));
            }
            dependencies.request(options, function (err, response, body) {
                if (err) {
                	reject(err);
                } else if (!response) {
                    reject(new Error('Bad response'));
                } else if (response.statusCode !== 200) {
                    reject(new Error(util.format('Bad status %s', response.statusCode)));
                } else {
                    try {
                        const bodyObj = JSON.parse(body);
                        resolve(bodyObj);
                    } catch (ex) {
                        reject(ex)
                    }
                }
            });
        }
    );
}

/**
 * enrich the objects given by fetchListOfProjects with measures
 * @type {fetchProjectsMeasures}
 */
module.exports.fetchProjectsMeasures = fetchProjectsMeasures;
function fetchProjectsMeasures(config, dependencies, projects) {

    // synch with a promise and recursion
    // with recursion only 1 req at a time, dilluting the load on SonarQube
    return new Promise(
        function (resolve, reject) {

            queryOneProject(resolve, reject, 0);
        }
    );

    function queryOneProject(resolve, reject, projectIndex) {
        if (projectIndex < projects.length) {
            // @see https://docs.sonarqube.org/display/SONAR/Metric+definitions
            path = util.format(
                '/api/measures/component_tree?baseComponentKey=%s' +
                '&metricKeys=quality_gate_details,alert_status,last_commit_date,critical_violations,blocker_violations,coverage,new_coverage',
                projects[projectIndex].k);
            if (config.showMajorViolations) {
                path += ',major_violations'
            }
            if (config.globalAuth[config.credentials].externalURL) {
                rootUrl = config.globalAuth[config.credentials].externalURL;
            } if (config.externalURL) {
                rootUrl = config.externalURL;
            } else {
                rootUrl = config.globalAuth[config.credentials].rootURL;
            }
            const projectUrl = rootUrl + '/dashboard?id=' + projects[projectIndex].k
            const options = computeRequestOptionsForSQ(config, path);
            dependencies.request(options, function (err, response, body) {
                    if (err) {
                        reject(err)
                    } else if (!response) {
                        reject(new Error('Bad response'));
                    } else if (response.statusCode !== 200) {
                        reject(new Error(util.format('Bad status %s', response.statusCode)));
                    } else {
                        try {
                            const bodyObj = JSON.parse(body);

                            projects[projectIndex].url = projectUrl;

                            // enrich existing project object
                            projects[projectIndex].measures = bodyObj.baseComponent.measures;

                            // recursion
                            queryOneProject(resolve, reject, projectIndex + 1);
                        } catch (ex) {
                            reject(ex);
                        }
                    }
                }
            );
        } else {
            resolve(projects);
        }
    }
}

module.exports.decodeProjectsMeasures = decodeProjectsMeasures;
function decodeProjectsMeasures(projects) {
    return new Promise(
        function (resolve, reject) {
            resolve(projects.map(function (projectObj) {

                var returned_project = {
                    key: projectObj.k
                    , name: projectObj.nm
                    , url: projectObj.url
                };

                // collect the values,
                projectObj.measures.forEach(function (projectMsr) {

                    const metricKey = projectMsr.metric;
                    var value;
                    switch (metricKey) {
                        case 'alert_status':
                            value = projectMsr.value;
                            break;
                        case 'quality_gate_details':
                            const data = JSON.parse(projectMsr.value);
                            // value is only the non OK measures
                            value = data.conditions.filter(function (condition) {
                                return condition.level !== 'OK';
                            });
                            break;
                        case 'last_commit_date':
                            value = parseInt(projectMsr.value, 10);
                            break;
                        case 'critical_violations':
                            value = parseInt(projectMsr.value, 10);
                            break;
                        case 'blocker_violations':
                            value = parseInt(projectMsr.value, 10);
                            break;
                        case 'new_coverage':
                            value = parseInt(projectMsr.value, 10);
                            break;
                        case 'coverage':
                            value = parseInt(projectMsr.value, 10);
                            break;
                        case 'major_violations':
                            value = parseInt(projectMsr.value, 10);
                            break;
                        
                        default:
                            reject(new Error('Invalid measure key :' + metricKey));
                            break;
                    }

                    returned_project[metricKey] = value;
                });

                return returned_project;

            }));
        });
}

module.exports.filterProjectsWithStatus = filterProjectsWithStatus;
function filterProjectsWithStatus(config, projects, timeHorizon, projectFilterFunction, metricFilterFunction) {

    return new Promise(function (resolve /*, reject*/) {

            // this will be returned
            var projectsInError = {};

            projects.forEach(function (project) {
                if (// only projet according to projectFilterFunction
                    projectFilterFunction(project)
                    // for less then XX days
                    &&
                    ( ! timeHorizon || project.last_commit_date >= timeHorizon )
                ) {
                    // pick the measures in ERROR
                    projectsInError[project.name] = {};
                    projectsInError[project.name].url = project.url;
                    projectsInError[project.name].last_commit_date = project.last_commit_date;
                    projectsInError[project.name].metrics = project.quality_gate_details.filter(function (oneMeasure) {
                        return metricFilterFunction(oneMeasure.level);
                    }).map(function (oneFaultyMeasure) {
                        return {
                            // multiply and divide by ten preserves from bad rounding
                            count: Math.round(oneFaultyMeasure.actual * 10.0) / 10.0,
                            metric: oneFaultyMeasure.metric
                        };

                    });
                    if (!config.onlyQualityGate) {
                        if (project.coverage && project.coverage < 50) {
                            projectsInError[project.name].metrics.push({
                                count: project.coverage,
                                metric: 'coverage'
                            });
                        }
                        if (project.critical_violations && project.critical_violations > 0) {
                            projectsInError[project.name].metrics.push({
                                count: project.critical_violations,
                                metric: 'critical_violations'
                            });
                        }
                        if (project.blocker_violations && project.blocker_violations > 0) {
                            projectsInError[project.name].metrics.push({
                                count: project.blocker_violations,
                                metric: 'blocker_violations'
                            });
                        }
                        if (config.showMajorViolations && project.major_violations && project.major_violations > 0) {
                            projectsInError[project.name].metrics.push({
                                count: project.major_violations,
                                metric: 'major_violations'
                            });
                        }
                    }
                }
            });

            resolve(projectsInError);
        }
    );
}
