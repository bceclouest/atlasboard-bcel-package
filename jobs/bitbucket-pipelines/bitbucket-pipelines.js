/**
 * Job: bitbucket-pipelines
 *
 * Expected configuration:
 *
 * ## PLEASE ADD AN EXAMPLE CONFIGURATION FOR YOUR JOB HERE
 * { 
 *   pipeline : [ 
 *     {
 *       serverUrl : '',
 *       user : '',
 *       password : '',
 *       credentials : '',
 *       repo : '',
 *       projectName: '', OU nameFilter: '',
 *       pullRequests: 'true,
 *       pipelineStatusFilter : 'FAILED', // SUCCESSFUL
 *       pipelineConfigFilter : 'enabled', // disabled
 *       pipelineCount : 10,
 *       branchFilter: '',
 *       ignoreBranch: ''
 *     }
 *   ]
 * }
 */

const api = require('./lib/bitbucket-api.js');

module.exports = {

  // dependencies: null,

  /**
   * Executed on job initialisation (only once)
   * @param config
   * @param dependencies
   */
  onInit: function (config, dependencies) {

    // this.dependencies = dependencies;
    /*
    This is a good place for initialisation logic, like registering routes in express:

    dependencies.logger.info('adding routes...');
    dependencies.app.route("/jobs/mycustomroute")
        .get(function (req, res) {
          res.end('So something useful here');
        });
    */
  },

  /**
   * Executed every interval
   * @param config
   * @param dependencies
   * @param jobCallback
   */
  onRun: function (config, dependencies, jobCallback) {

    if (config.credentials) {
      config.serverUrl = config.globalAuth[config.credentials].serverUrl;
      config.user = config.globalAuth[config.credentials].username;
      config.password = config.globalAuth[config.credentials].password;
    }
    if (!config.serverUrl) {
      if (config.debug) {
        console.log("Missing serverUrl");
      }
      jobCallback("Missing serverUrl", { title: config.title, html: "Configuration problem" });
      return ;
    }

    api.getRepositories(config, config.serverUrl + '/repositories/' + config.repo + '/')
        .then(function(repositories) {
          if (config.pullRequests) {
            return api.fetchPullRequests(config, dependencies, repositories);
          }
          return api.fetchPipelineStatus(config, dependencies, repositories);
        })
        .then(function(repositories) {
            return api.filterRepositories(config, repositories, config.pipelineStatusFilter, repo => {
                repo.pipelines = repo.pipelines.filter( build => {
                  return build.result === config.pipelineStatusFilter;
                });
                return !(repo.pipelines.length == 0);
            });
        })
        .then(function(repositories) {
          if (config.debug) {
            console.log("Repositories for " + config.title + ": " + JSON.stringify(repositories));
          }
          jobCallback('', { title : config.title, repositories : repositories });
        })
        .catch(function (e) {
            console.log(JSON.stringify(e));
        });
  }
};