
const https = require('https');
const url = require('url');
const fs = require('fs');

function httpRequest(config, requestedUrl, method, resolve, reject, body) {
    if (config.debug) {
        // console.log(config);
        // console.log(requestedUrl);
        // console.log(method);
        // console.log(resolve);
        // console.log(reject);
        // console.log(body);
        console.log('Request ' + method + ': ' + requestedUrl);
    }
    var options = url.parse(requestedUrl);
    options.headers = {
        'Content-Type': 'application/json'
    };
    if (config.user) {
        options.headers['Authorization'] = 'Basic ' + new Buffer(config.user + ':' + config.password).toString('base64');
    }
    options.method = method;
    if (config.trace) {
        console.log(JSON.stringify(options));
    }
    const request = https.request(options, function (response) {
        if (config.debug) {
            console.log('statusCode: ' + response.statusCode);
        }
        if (response.statusCode !== 200) {
            reject({ statusCode: response.statusCode, message: response.statusMessage });
            return ;
        }
        var jsonString = "";
        response
            .on('data', function(data) {
                jsonString += data;
            })
            .on('end', function() {
                if (config.trace) {
                    console.log(jsonString);
                }
                //here we have the full response, html or json object
                var jsonResponse = JSON.parse(jsonString);
                // console.log(jsonResponse);
                resolve(jsonResponse);
            })
            .on('error', function(e) {
                console.log("Got error: " + e.message);
                reject(e);
            });
    });
    if (body) {
        if (config.debug) {
            console.log('Payload: ' + body);
        }
        request.write(body);
    }
    request.end();
}

function httpGetRequest(config, requestedUrl, resolve, reject) {
    httpRequest(config, requestedUrl, 'GET', resolve, reject);
}

function httpPutRequest(config, requestedUrl, resolve, reject, body) {
    httpRequest(config, requestedUrl, 'PUT', resolve, reject, body);
}

exports.fetchRepositories = function(config, repoUrl, repositories) {
    return new Promise(
        function (resolve, reject) {
            if (config.projectName) {
                repoUrl += '?q=project.name="' + config.projectName + '"';
            } else if (config.nameFilter) {
                repoUrl += '?q=name~"' + config.nameFilter + '"';
            }
            fetchOnePage(repoUrl, resolve, reject);
        }
    );

    function fetchOnePage(pageUrl, resolve, reject) {
        httpGetRequest(config, pageUrl, function (jsonResponse) {
            if (!jsonResponse.values) {
                reject("No repository found");
                return ;
            }
            jsonResponse.values.forEach(function(repository) {
                repositories.push({
                    name: repository.name,
                    url: repository.links.self.href
                });
            }, this);
            if (jsonResponse.next) {
                fetchOnePage(jsonResponse.next, resolve, reject);
            } else {
                resolve(repositories);
            }
        }, reject);
    };
};

exports.filterRepositories = function(config, repositories, configProperty, filterFunction) {
    return new Promise(
        function (resolve, reject) {
            var filteredRepos = repositories;
            if (configProperty) {
                if (config.debug) {
                    console.log('Filtering with: ' + configProperty);
                }
                filteredRepos = repositories.filter(filterFunction);
            } else {
                if (config.debug) {
                    console.log('No filter');
                }
            }
            resolve(filteredRepos);
        }
    );
}

exports.fetchPipelineConfigStatus = function(config, repositories) {
    return new Promise(
        function (resolve, reject) {
            fetchOnePipelineConfig(0, resolve, reject);
        }
    );

    function fetchOnePipelineConfig(index, resolve, reject) {
        if (index > repositories.length - 1) {
            resolve(repositories);
            return ;
        }
        var currentRepo = repositories[index];
        httpGetRequest(config, currentRepo.url + '/pullrequests_config', function (jsonResponse) {
            currentRepo.pipelineConfigStatus = jsonResponse.enabled ? 'enabled' : 'disabled';
            currentRepo.pipelineConfig = jsonResponse.enabled;
            fetchOnePipelineConfig(index + 1, resolve, reject);
        }, function (e) {
            if (e.statusCode === 404) {
                currentRepo.pipelineConfigStatus = 'disabled';
                currentRepo.pipelineConfig = false;
                fetchOnePipelineConfig(index + 1, resolve, reject);
            } else {
                reject(e);
            }
        } );
    };
};

exports.fetchPipelineStatus = function(config, dependencies, repositories) {
    return new Promise(
        function (resolve, reject) {
            fetchOnePipelineStatus(0, resolve, reject);
        }
    );

    function fetchOnePipelineStatus(index, resolve, reject) {
        if (index > repositories.length - 1) {
            resolve(repositories);
            return ;
        }
        var currentRepo = repositories[index];
        pipelineUrl = currentRepo.url + '/pipelines/?';
        pipelineUrl += '&sort=-created_on';
        httpGetRequest(config, pipelineUrl, function (jsonResponse) {
            var limit = 10;
            if (config.pipelineCount) {
                limit = config.pipelineCount > 10 ? 10 : config.pipelineCount;
            }
            pipelines = jsonResponse.values.slice(0, limit);
            pipelines = removeDuplicatesBy(pipeline => pipeline.target.ref_name, pipelines);
            if (config.ignoreBranch) {
                pipelines = pipelines
                .filter(function(pipeline) {
                    return (pipeline.target.ref_name && pipeline.target.ref_name !== config.ignoreBranch);
                });
            }
            if (config.branchFilter) {
                if (config.debug) {
                    console.log('Filtering branches with: ' + config.branchFilter);
                }
                pipelines = pipelines
                    .filter(function(pipeline) {
                        return (pipeline.target.ref_name && pipeline.target.ref_name === config.branchFilter);
                    })
                    .slice(0, 1);
            // } else {
            //     pipelines = removeDuplicatesBy(pipeline => pipeline.target.ref_name, pipelines);
            }
            currentRepo.pipelines = pipelines.map( build => {
                return {
                    date: dependencies.moment(new Date(build.created_on)).fromNow(),
                    branch: build.target.ref_name,
                    duration: build.build_seconds_used,
                    status: build.state.name,
                    result: build.state.result ? build.state.result.name : "RUNNING", //build.state.stage.name,
                    branch: build.target.ref_name,
                    url: build.repository.links.html.href + '/addon/pipelines/home#!/results/' + build.build_number
                };
            });
            fetchOnePipelineStatus(index + 1, resolve, reject);
        }, function (e) {
            if (e.statusCode === 404) {
                currentRepo.pipelines = [];
                fetchOnePipelineStatus(index + 1, resolve, reject);
            } else {
                reject(JSON.stringify(e));
            }
        } );
    }
};

exports.fetchPullRequests = function(config, dependencies, repositories) {
    return new Promise(
        function (resolve, reject) {
            fetchOnePullRequest(0, resolve, reject);
        }
    );
    /**
     * Récupére tous les pull requests d'un repository
     * @param {int} index l'index courant du repository à récupérer
     * @param {function} resolve la fonction callback ok
     * @param {function} reject la fonction callback d'erreur
     */
    function fetchOnePullRequest(index, resolve, reject) {
        if (index > repositories.length - 1) {
            resolve(repositories);
            return ;
        }
        var currentRepo = repositories[index];
        pullrequestUrl = currentRepo.url + '/pullrequests/?';
        pullrequestUrl += '&sort=-created_on';
        httpGetRequest(config, pullrequestUrl, function (jsonResponse) {
            var limit = 10;
            pullrequests = jsonResponse.values.slice(0, limit);
            pullrequests = pullrequests
            if (config.branchFilter) {
                if (config.debug) {
                    console.log('Filtering PR destination branches with: ' + config.branchFilter);
                }
                pullrequests = pullrequests
                    .filter(function(pullrequest) {
                        return (pullrequest.destination.branch.name === config.branchFilter);
                    });
            }
            currentRepo.pullrequests = pullrequests.map( pr => {
                return {
                    date: dependencies.moment(new Date(pr.created_on)).fromNow(),
                    title: pr.title,
                    status: pr.state,
                    author: pr.author.display_name,
                    from: pr.source.branch.name,
                    to: pr.destination.branch.name,
                    url: pr.links.html.href
                };
            });
            fetchOnePullRequest(index + 1, resolve, reject);
        }, function (e) {
            reject(JSON.stringify(e));
        } );
    }
};

function removeDuplicatesBy(keyFn, array) {
    var mySet = new Set();
    return array.filter(function(x) {
        var key = keyFn(x), isNew = !mySet.has(key);
        if (isNew) mySet.add(key);
        return isNew;
    });
}

function readFile(path) {
    return new Promise(
        function (resolve, reject) {
            fs.readFile(path, 'utf8', function (err, content) {
                if (err) {
                  throw new Error(err);
                }
                resolve(content);
              });
        }
    );
};

exports.readJsonFile = function(path) {
    return readFile(path).then( content => JSON.parse(content));
};

function getTemplate(templateName) {
    return readFile('templates/' + templateName + '.json');
};

exports.enablepullrequests = function(repositories) {
    getTemplate('enable_pipeline').then(function(template) {
        repositories.forEach(function(repo) {
            httpPutRequest(repo.url + '/pullrequests_config', jsonResponse => {
                console.log(jsonResponse);
            }, function (e) {
                console.log('Error while activating pipeline for: ' + repo.name + ' -> ' + JSON.stringify(e));
                throw new Error(JSON.stringify(e));
            }, template)
        }, this);
    });
};

exports.getRepositories = function(config, repoUrl) {
    return module.exports.fetchRepositories(config, repoUrl, [])
};

/**
 * Prototype de fonction de remplacement dans un json
 */
// if (properties) {
// var replacer = function (key, val) {
//     if (typeof val === 'string' && val.match(/^{(.+)}$/)) {
//       return properties[val.replace(/[{|}]/g, '')]
//     }
//     return val;
//   }; 
// JSON.stringify(JSON.parse(template), replacer);
// }
