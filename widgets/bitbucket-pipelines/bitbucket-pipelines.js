widget = {
  //runs when we receive data from the job
  onData: function (el, data) {
    var title = 'BUILDS';
    if (data.title) {
      title = data.title;
    }
    var html = '<h2>' + title + '</h2>';
    var hasContent = false;
    $.each(data.repositories, function (index, repository) {
      var currentProjectName = repository.name;
      if (repository.pipelines) {
        hasContent = true;
        repository.pipelines.forEach(function (pipeline) {
          html += '<div class="build build-' + pipeline.result.toLowerCase() + '">' ;
          html += '<div class="project-name"><a href="'+ pipeline.url +'">' + currentProjectName + '</a></div>';
          html += '<div class="secondline">';
          html += '<div class="branch">' + pipeline.branch + '</div>';
          html += '<div class="build-time">' + pipeline.date + '</div>';
          html += '</div>';
          html += '</div>';
        });
      } else if (repository.pullrequests) {
        hasContent = true;
        repository.pullrequests.forEach(function (pr) {
          html += '<div class="build build-running">' ;
          html += '<div class="project-name"><a href="'+ pr.url +'">' + currentProjectName + '</a></div>';
          html += '<div class="secondline">';
          html += '<div class="branch">' + pr.from + '</div>';
          html += '<div class="build-time">' + pr.date + '</div>';
          html += '</div>';
          html += '</div>';
        });
      }
    });
    if (hasContent == false) {
      html += '<div class="build build-successful">' ;
      html += '<div class="project-name"> No failed builds </div>';
      html += '<div class="secondline">';
      html += '<div class="branch">Last check</div>';
      html += '<div class="build-time">' + new Date() + '</div>';
      html += '</div>';
      html += '</div>';
    }
    
    $('.content', el).html(html);
  }
};