widget = {

    onData: function (el, data) {

        const countCSS = data.statusOk === true ? 'countGreen' : 'countRed';
        const statusName = data.statusOk === true ? 'statusNameGreen' : 'statusNameRed';

        $('.count', el).html(`<div class='${countCSS}'>${data.projects.length}<div class='title'>${data.title}</div></div>`);

        $('.content', el).html('');
        data.projects.forEach(function (project) {
            var name = project.name;
            var html = '<div class=\'status\'><div class=\'statusName ' + statusName + '\'>' + 
                '<a href="' + project.url + '">' + 
                name 
                + '</a>'
                + '</div></div>';
            $('.content', el).append(html);
            var index;
            for (index = 0; index < project.metrics.length; index++) {
                html = '<div class=\'countRedSmall\'>' + project.metrics[index].count
                    + '<div class=\'metric\'>' + project.metrics[index].metric
                    + '</div></div>';
                $('.content', el).append(html);
            }
        });
    }
};
